package com.fuga.space.model;

import com.fuga.space.util.PrintUtils;
import lombok.Builder;
import lombok.Getter;

import static com.fuga.space.util.AssertUtils.assertExists;
import static com.fuga.space.util.AssertUtils.assertRectangularShape;

@Getter
@Builder
public class Invader implements Printable {

    private static final String title_prefix = "Invader";
    private static final String blank_space = " ";

    private final String name;
    private final char[][] shape;

    public Invader(String name, char[][] shape) {
        assertExists(shape, "This invader is destroyed and empty! No sign of aggression, Captain...");
        assertRectangularShape(shape, "Wow, this invader is wasted! The shape should be rectangular!");
        this.name = name;
        this.shape = shape;
    }

    public String getTitle() {
        return title_prefix + blank_space + name;
    }

    public int getSize() {
        return shape.length * shape[0].length;
    }

    public int getWidth() {
        return shape.length;
    }

    public int getHeight() {
        return shape[0].length;
    }

    @Override
    public void print() {
        System.out.println("~~~~~~~~~" + getTitle() + "~~~~~~~~~");
        PrintUtils.print(shape);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
}
