package com.fuga.space.model;

import com.fuga.space.util.PrintUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Iterator;
import java.util.Random;

import static com.fuga.space.util.AssertUtils.assertExists;
import static com.fuga.space.util.AssertUtils.assertRectangularShape;

@Getter
public class RadarImage implements Iterable<RadarImage.Index>, Printable {

    static final int shot_radius = 4;
    static final char empty_space_symbol = '-';
    private static final int shot_max_damage = 5;
    private static final Random random = new Random();

    private final char[][] readings;

    public RadarImage(char[][] readings) {
        assertExists(readings, "This radar is broken! No readings arrive, Captain...");
        assertRectangularShape(readings, "Wow, this Radar is giving weird results! The scan should be rectangular!");
        this.readings = readings;
    }

    public int getWidth() {
        return readings.length;
    }

    public int getHeight() {
        return readings[0].length;
    }

    public void shoot(int x, int y) {
        for (int i = 0; i < shot_max_damage; i++) {
            int xCoordinate = x + random.nextInt(shot_radius);
            int yCoordinate = y + random.nextInt(shot_radius);
            if (withinImage(xCoordinate, yCoordinate)) {
                readings[xCoordinate][yCoordinate] = empty_space_symbol;
            }
        }
    }

    private boolean withinImage(int xCoordinate, int yCoordinate) {
        return xCoordinate > 0 && xCoordinate < getHeight() && yCoordinate > 0 && yCoordinate < getWidth();
    }

    @Override
    public Iterator<Index> iterator() {
        return new IndexIterator(readings);
    }

    @Override
    public void print() {
        System.out.print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        PrintUtils.print(readings);
        System.out.println();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }


    @Getter
    @AllArgsConstructor
    public class Index {

        private final int xPosition, yPosition;

        public char getSymbol(int x, int y) {
            return readings[xPosition + x][yPosition + y];
        }

    }

    public class IndexIterator implements Iterator<Index> {

        private int i = 0, j = -1;
        private final int maxI, maxJ;

        IndexIterator(char[][] readings) {
            maxI = readings.length - 1;
            maxJ = readings[0].length - 1;
        }

        @Override
        public boolean hasNext() {
            return i < maxI || j < maxJ;
        }

        @Override
        public Index next() {
            if (j < maxJ) {
                j++;
            } else if (i < maxI) {
                i++;
                j = 0;
            } else {
                throw new IllegalStateException("No way I can iterate more, Captain!");
            }
            return new Index(i, j);
        }
    }

}
