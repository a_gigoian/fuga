package com.fuga.space.model;

import lombok.Builder;
import lombok.Getter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
public class DetectionResult implements Printable {

    private final String spaceObjectName;
    private final List<PointResult> results;

    public DetectionResult(String spaceObjectName) {
        this.spaceObjectName = spaceObjectName;
        this.results = new ArrayList<>();
    }

    public void add(int x, int y, double probability, double quality) {
        results.add(PointResult.builder()
                .x(x)
                .y(y)
                .probability(probability)
                .dataQuality(quality)
                .build()
        );
    }

    public Optional<PointResult> get(int x, int y) {
        return results.stream().filter(r -> (x == r.getX() && y == r.getY())).findFirst();
    }

    @Override
    public void print() {
        System.out.println("Detection results for: " + spaceObjectName + " ready, Captain!");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        results.forEach(System.out::println);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }

    @Getter
    @Builder
    public static class PointResult {

        private static final int max_data_quality = 1;
        private static final DecimalFormat format = new DecimalFormat("0.00");

        private final int x;
        private final int y;
        private final double probability;
        private final double dataQuality;

        @Override
        public String toString() {
            return "[" + x + ";" + y + "]:" + format.format(probability) +
                    (dataQuality < max_data_quality ? " dataQuality: " + dataQuality + ";" : ";");
        }
    }

}
