package com.fuga.space.model;

public interface Printable {

    void print();

}
