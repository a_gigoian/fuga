package com.fuga.space;

import com.fuga.space.detector.InvaderDetector;
import com.fuga.space.model.Invader;
import com.fuga.space.model.RadarImage;
import com.fuga.space.util.PrintUtils;
import org.apache.commons.io.FilenameUtils;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.fuga.space.util.FileUtils.listFiles;
import static com.fuga.space.util.FileUtils.readFile;
import static java.util.stream.Collectors.toList;

public class Application {

    private static final String radar_data = "radar/radar_data.txt";
    private static final String spaceship = "spaceship/ship.txt";
    private static final String invaders_store = "invaders/";
    private static final Pattern coordinatesPattern = Pattern.compile("\\d+:\\d+");
    private static final String quit_input = "q";
    private static final String print_input = "p";

    public static void main(String[] args) {
        List<Invader> invaders = initInvaders();
        RadarImage radarImage = initRadar();
        InvaderDetector detector = initDetector(radarImage);

        printStory(invaders);

        play(radarImage, invaders, detector);

    }

    private static void play(RadarImage radarImage, List<Invader> invaders, InvaderDetector detector) {
        while (true) {
            Scanner in = new Scanner(System.in);
            String userInput = in.nextLine();

            if (quit_input.equalsIgnoreCase(userInput)) {
                quit();
            }
            if (print_input.equalsIgnoreCase(userInput)) {
                printRadarDetection(radarImage, invaders, detector);
                continue;
            }

            if (coordinatesPattern.matcher(userInput).matches()) {
                printShot(userInput);
                shoot(radarImage, userInput);
                printDetection(invaders, detector);
                continue;
            }
            fallback();
        }
    }

    private static void shoot(RadarImage radarImage, String userInput) {
        String[] shot = userInput.split(":");
        radarImage.shoot(Integer.valueOf(shot[0]), Integer.valueOf(shot[1]));
    }

    private static void fallback() {
        System.out.println("Are you challenged mentally, Captain?");
    }

    private static void printShot(String userInput) {
        System.out.println("                *  *   * * **  * *");
        System.out.println("Shot!!! " + userInput + " pewwwww ...>===D *  *    *");
        System.out.println("              *    *  *    *  *");
    }

    private static void printRadarDetection(RadarImage radarImage, List<Invader> invaders, InvaderDetector detector) {
        radarImage.print();
        printDetection(invaders, detector);
    }

    private static void printDetection(List<Invader> invaders, InvaderDetector detector) {
        invaders.forEach(invader ->
                detector.detect(invader).print()
        );
    }

    private static void quit() {
        System.out.println("It was an honor, Captain!");
        System.exit(0);
    }

    private static void printStory(List<Invader> invaders) {
        System.out.println("In a Galaxy far far away...");
        System.out.println("At a Time that has been forgotten...");
        System.out.println("A lonely Spaceship was gathering valuable Suluguni-112, when space invaders showed up on the radar!");
        PrintUtils.print(readFile(spaceship));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        invaders.forEach(Invader::print);
        System.out.println("To Launch a Missile! Type the coordinates in the following format: x:y");
        System.out.println("To see the radar image! Type 'P'");
        System.out.println("To Quit! Type 'Q'");
    }

    private static RadarImage initRadar() {
        return new RadarImage(readFile(radar_data));
    }

    private static List<Invader> initInvaders() {
        return Stream.of(listFiles(invaders_store))
                .map(
                        i -> Invader.builder()
                                .name(FilenameUtils.removeExtension(i.getName()))
                                .shape(readFile(i))
                                .build()
                ).collect(toList());
    }

    private static InvaderDetector initDetector(RadarImage radarImage) {
        return new InvaderDetector(radarImage);
    }
}
