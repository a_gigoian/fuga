package com.fuga.space.util;

import lombok.experimental.UtilityClass;

import java.util.stream.Stream;

@UtilityClass
public class AssertUtils {

    public static void assertExists(char[][] readings, String message) {
        if (readings == null || readings.length <= 0 || readings[0].length <= 0) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void assertRectangularShape(char[][] readings, String message) {
        int length = readings[0].length;
        Stream.of(readings).filter(line -> line.length != length)
                .findFirst()
                .ifPresent(v -> {
                            throw new IllegalArgumentException(message);
                        }
                );
    }

}
