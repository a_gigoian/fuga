package com.fuga.space.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class PrintUtils {

    public static void print(char[][] print) {
        for (char[] line : print) {
            System.out.println();
            for (char symbol : line) {
                System.out.print(symbol);
            }
        }
        System.out.println();
    }

}
