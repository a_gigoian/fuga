package com.fuga.space.util;

import com.fuga.space.Application;
import lombok.experimental.UtilityClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@UtilityClass
public class FileUtils {

    public static File[] listFiles(String dir) {
        return getFileChecked(dir).listFiles();
    }

    public static char[][] readFile(String fileName) {
        return readFile(getFileChecked(fileName));
    }

    public static char[][] readFile(File file) {
        try {
            Scanner scanner = new Scanner(file);
            List<char[]> lines = new ArrayList<>();
            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine().toCharArray());
            }
            char[][] result = new char[lines.size()][];
            for (int i = 0; i < lines.size(); i++) {
                result[i] = lines.get(i);
            }
            return result;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Please don't give me files I can't find :)", e);
        }
    }

    private static File getFileChecked(String path) {
        ClassLoader classLoader = Application.class.getClassLoader();
        Optional<URL> resource = Optional.ofNullable(classLoader.getResource(path));
        return new File(resource.orElseThrow(() -> new IllegalArgumentException("No resource found for: " + path))
                .getFile());
    }

}
