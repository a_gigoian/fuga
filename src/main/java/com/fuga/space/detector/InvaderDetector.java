package com.fuga.space.detector;

import com.fuga.space.model.DetectionResult;
import com.fuga.space.model.Invader;
import com.fuga.space.model.RadarImage;
import javafx.util.Pair;
import lombok.AllArgsConstructor;

import static java.lang.Math.max;
import static java.lang.Math.min;

@AllArgsConstructor
public class InvaderDetector {

    private static final double probability_threshold = 0.5;
    private static final double quality_threshold = 0.5;

    private final RadarImage image;

    public DetectionResult detect(Invader invader) {
        DetectionResult result = new DetectionResult(invader.getTitle());
        image.forEach(index -> {
                    Pair<Double, Double> p = checkInvader(index, invader);
                    if (meetsThresholds(p)) {
                        result.add(index.getXPosition(), index.getYPosition(), p.getKey(), p.getValue());
                    }
                }
        );
        return result;
    }

    private boolean meetsThresholds(Pair<Double, Double> p) {
        return p.getKey() > probability_threshold && p.getValue() > quality_threshold;
    }

    private Pair<Double, Double> checkInvader(RadarImage.Index index, Invader invader) {
        int maxI = min(invader.getWidth(), image.getWidth() - index.getXPosition());
        int maxJ = min(invader.getHeight(), image.getHeight() - index.getYPosition());
        int frameSize = maxI * maxJ;
        int maxScore = min(invader.getSize(), frameSize);
        double dataQuality = maxScore / (double) invader.getSize();

        int score = 0;
        for (int i = 0; i < maxI; i++) {
            for (int j = 0; j < maxJ; j++) {
                score += match(index.getSymbol(i, j), invader.getShape()[i][j]);
            }
        }
        double probability = max(score / (double) maxScore, 0);
        return new Pair<>(probability, dataQuality);
    }

    private int match(char v1, char v2) {
        return v1 == v2 ? 1 : -1;
    }


}