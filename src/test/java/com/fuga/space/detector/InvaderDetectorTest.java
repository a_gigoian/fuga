package com.fuga.space.detector;

import com.fuga.space.model.DetectionResult;
import com.fuga.space.model.Invader;
import com.fuga.space.model.RadarImage;
import org.junit.Test;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class InvaderDetectorTest {

    private static final double delta = 0.01;

    @Test
    public void test_detect_fullMatch() {
        char[][] readings = new char[3][3];
        RadarImage image = new RadarImage(readings);
        InvaderDetector invaderDetector = new InvaderDetector(image);

        char[][] invaderShape = new char[3][3];
        Invader invader = new Invader("armen", invaderShape);

        Arrays.fill(readings[0], '-');
        Arrays.fill(readings[1], 'o');
        Arrays.fill(readings[2], '-');

        Arrays.fill(invaderShape[0], '-');
        Arrays.fill(invaderShape[1], 'o');
        Arrays.fill(invaderShape[2], '-');

        DetectionResult result = invaderDetector.detect(invader);

        assertEquals(invader.getTitle(), result.getSpaceObjectName());
        assertFalse(result.getResults().isEmpty());

        Optional<DetectionResult.PointResult> pointResult00 = result.get(0, 0);
        double quality00 = 1;
        double probability00 = 1;

        assertTrue(pointResult00.isPresent());
        assertEquals(quality00, pointResult00.get().getDataQuality(), delta);
        assertEquals(probability00, pointResult00.get().getProbability(), delta);

        Optional<DetectionResult.PointResult> pointResult12 = result.get(0, 1);
        double quality12 = 0.66;
        double probability12 = 1;

        assertTrue(pointResult12.isPresent());
        assertEquals(quality12, pointResult12.get().getDataQuality(), delta);
        assertEquals(probability12, pointResult12.get().getProbability(), delta);

        Optional<DetectionResult.PointResult> pointResult33 = result.get(1, 1);

        assertFalse(pointResult33.isPresent());
    }

    @Test
    public void test_detect_partMatch() {
        char[][] readings = new char[3][3];
        RadarImage image = new RadarImage(readings);
        InvaderDetector invaderDetector = new InvaderDetector(image);

        char[][] invaderShape = new char[3][3];
        Invader invader = new Invader("armen", invaderShape);

        Arrays.fill(readings[0], '-');
        Arrays.fill(readings[1], 'o');
        Arrays.fill(readings[2], '-');

        Arrays.fill(invaderShape[0], '-');
        invaderShape[1][0] = '-';
        invaderShape[1][1] = 'o';
        invaderShape[1][2] = '-';
        Arrays.fill(invaderShape[2], '-');

        DetectionResult result = invaderDetector.detect(invader);

        assertEquals(invader.getTitle(), result.getSpaceObjectName());
        assertFalse(result.getResults().isEmpty());

        Optional<DetectionResult.PointResult> pointResult00 = result.get(0, 0);
        double quality00 = 1;
        double probability00 = 0.56;

        assertTrue(pointResult00.isPresent());
        assertEquals(quality00, pointResult00.get().getDataQuality(), delta);
        assertEquals(probability00, pointResult00.get().getProbability(), delta);

        Optional<DetectionResult.PointResult> pointResult12 = result.get(0, 1);
        double quality12 = 0.66;
        double probability12 = 0.67;

        assertTrue(pointResult12.isPresent());
        assertEquals(quality12, pointResult12.get().getDataQuality(), delta);
        assertEquals(probability12, pointResult12.get().getProbability(), delta);

        Optional<DetectionResult.PointResult> pointResult33 = result.get(1, 1);

        assertFalse(pointResult33.isPresent());
    }

    @Test
    public void test_detect_noMatch() {
        char[][] readings = new char[3][3];
        RadarImage image = new RadarImage(readings);
        InvaderDetector invaderDetector = new InvaderDetector(image);

        char[][] invaderShape = new char[3][3];
        Invader invader = new Invader("armen", invaderShape);

        Arrays.fill(readings[0], '-');
        Arrays.fill(readings[1], '-');
        Arrays.fill(readings[2], '-');

        Arrays.fill(invaderShape[0], 'o');
        Arrays.fill(invaderShape[1], 'o');
        Arrays.fill(invaderShape[2], 'o');

        DetectionResult result = invaderDetector.detect(invader);

        assertEquals(invader.getTitle(), result.getSpaceObjectName());
        assertTrue(result.getResults().isEmpty());
    }


}