package com.fuga.space.util;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class AssertUtilsTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void test_assertExists_no() {
        String message = "why?";
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(message);

        AssertUtils.assertExists(new char[0][0], message);
    }

    @Test
    public void test_assertExists_yes() {
        AssertUtils.assertExists(new char[1][1], "");
    }


    @Test
    public void test_assertRectangularShape_no() {
        String message = "omg!";
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(message);

        char[][] readings = new char[2][];
        readings[0] = new char[1];
        readings[1] = new char[2];
        AssertUtils.assertRectangularShape(readings, message);
    }

    @Test
    public void test_assertRectangularShape_yes() {
        AssertUtils.assertRectangularShape(new char[2][5], "");
    }

}
