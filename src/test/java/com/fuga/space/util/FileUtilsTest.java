package com.fuga.space.util;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class FileUtilsTest {

    @Test
    public void test_listFiles_dirPresent() {
        File[] result = FileUtils.listFiles("testdir");
        assertEquals(2, result.length);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_listFiles_dirAbsent() {
        FileUtils.listFiles("testdir2");
    }

    @Test
    public void test_readFile_present() {
        char[][] result = FileUtils.readFile("testdir/testfile2.txt");
        assertEquals(3, result.length);
        assertEquals(3, result[0].length);
        assertEquals("---", new String(result[0]));
        assertEquals(":-)", new String(result[1]));
        assertEquals("---", new String(result[2]));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_readFile_notPresent() {
        FileUtils.readFile("testdir/testfile3.txt");
    }

}
