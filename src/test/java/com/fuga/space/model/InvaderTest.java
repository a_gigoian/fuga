package com.fuga.space.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InvaderTest {

    @Test(expected = IllegalArgumentException.class)
    public void test_create_empty() {
        new Invader("", new char[0][0]);
    }

    @Test
    public void test_create_ok() {
        char[][] shape = new char[2][2];
        Invader invader = new Invader("", shape);

        assertEquals(4, invader.getSize());
    }

    @Test
    public void test_getTitle() {
        char[][] shape = new char[2][2];
        Invader invader = new Invader("Zeus", shape);

        assertEquals("Invader Zeus", invader.getTitle());
    }

}
