package com.fuga.space.model;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DetectionResultTest {

    @Test
    public void test_add() {
        DetectionResult result = new DetectionResult("asterix");
        int x = 1;
        int y = 2;
        double probability = 0.7;
        double quality = 0.8;
        result.add(x, y, probability, quality);

        assertFalse(result.getResults().isEmpty());
        DetectionResult.PointResult p = result.getResults().get(0);
        assertEquals(x, p.getX());
        assertEquals(y, p.getY());
        assertEquals(probability, p.getProbability(), 0);
        assertEquals(quality, p.getDataQuality(), 0);
    }

    @Test
    public void test_get_present() {
        DetectionResult result = new DetectionResult("asterix");
        int x = 1;
        int y = 2;
        double probability = 0.7;
        double quality = 0.8;
        DetectionResult.PointResult p = DetectionResult.PointResult.builder()
                .x(x)
                .y(y)
                .probability(probability)
                .dataQuality(quality)
                .build();

        result.getResults().add(p);

        Optional<DetectionResult.PointResult> pResult = result.get(x, y);
        assertTrue(pResult.isPresent());
        assertEquals(p, pResult.get());
    }

    @Test
    public void test_get_absent() {
        DetectionResult result = new DetectionResult("asterix");
        Optional<DetectionResult.PointResult> pResult = result.get(1, 1);
        assertFalse(pResult.isPresent());
    }
}