package com.fuga.space.model;

import org.junit.Test;

import java.util.Arrays;

import static com.fuga.space.model.RadarImage.empty_space_symbol;
import static com.fuga.space.model.RadarImage.shot_radius;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;


public class RadarImageTest {

    @Test(expected = IllegalArgumentException.class)
    public void test_create_weirdShape() {
        char[][] readings = new char[2][];
        readings[0] = new char[2];
        readings[1] = new char[1];
        new RadarImage(readings);
    }

    @Test
    public void test_shoot() {
        char[][] readings = new char[shot_radius + 1][shot_radius + 1];
        for (int i = 0; i < shot_radius + 1; i++) {
            Arrays.fill(readings[i], 'o');
        }
        Integer emptySpaces = countEmptySpaces(readings);

        RadarImage image = new RadarImage(readings);
        image.shoot(0, 0);

        assertThat(emptySpaces, lessThan(countEmptySpaces(image.getReadings())));
    }

    private Integer countEmptySpaces(char[][] readings) {
        Integer count = 0;
        for (char[] line : readings) {
            for (char symbol : line) {
                if (empty_space_symbol == symbol) {
                    count++;
                }
            }
        }
        return count;
    }

}
