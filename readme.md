## Hi Captain! ##

To build and test the application run
> mvn clean package

To start the application run
> mvn exec:java

Live long and prosper!